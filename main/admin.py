from django.contrib import admin
from .models import PostArticle, PostAuthor

# Register your models here.
admin.site.register(PostArticle, list_display=('publication_date', 'title', 'author'), ordering=['-publication_date', 'title'])
admin.site.register(PostAuthor, list_display=('name', 'profile_page'), ordering=['name'])
