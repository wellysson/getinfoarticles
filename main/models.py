from django.db import models
from django.conf import settings
from datetime import datetime, date

# Create your models here.
class PostArticle(models.Model):
    title = models.CharField(max_length=150)
    website = models.CharField(max_length=250)
    author = models.ForeignKey('PostAuthor', on_delete=models.CASCADE)
    author_name = models.CharField(max_length=150, default=None)
    author_profile_page = models.CharField(max_length=250)
    publication_date = models.DateField()
    description = models.TextField()
    image_path = models.CharField(max_length=250)

    def publish(self):
        self.save()

    def __str__(self):
        return self.title

class PostAuthor(models.Model):
    name = models.CharField(max_length=250, default=None)
    profile_page = models.CharField(max_length=250)
    about_author = models.TextField()
    twitter_page = models.CharField(max_length=250)
    linkedin_page = models.CharField(max_length=250)
    crunchbase_page = models.CharField(max_length=250)

    def publish(self):
        self.save()

    def __str__(self):
        return self.name
