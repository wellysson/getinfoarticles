from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.http import require_http_methods
import requests
from bs4 import BeautifulSoup
from datetime import date
from .models import PostArticle, PostAuthor

def search_for_articles():
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    page = requests.get("https://techcrunch.com/")
    max_pages_first, force_leave = 4, False
    authors_ids = PostAuthor.objects.all()
    articles_ids = PostArticle.objects.all()
    look_new_articles = False if len(articles_ids) == 0 else True
    count_new_articles, count_new_authors = 0, 0

    while True:
        soup = BeautifulSoup(page.content, 'html.parser')

        for content in soup.body.find_all(class_="post-block"):
            title = content.find("a", class_="post-block__title__link")
            title = title.string.strip() if title.string != None else None
            if exist_article(title):
                force_leave = True
                break

            river_byline = content.find("div", class_="river-byline")
            publication_date = river_byline.time.string.strip().split(",")
            month_day = publication_date[0].split()
            publication_date = date(int(publication_date[1]), months.index(month_day[0]) + 1, int(month_day[1]))
            description = content.find("div", class_="post-block__content").string.strip()
            author_name = river_byline.a.string.strip() if river_byline.a != None and river_byline.a.string != None else None
            author_profile_page = river_byline.a.get("href") if river_byline.a != None else None

            # create author
            if len(authors_ids) == 0 or not exist_author(author_name):
                author_page = requests.get(author_profile_page)
                author_soup = BeautifulSoup(author_page.content, 'html.parser')
                author_profile_section = author_soup.body.find("section", class_="author-profile")
                if author_profile_section == None:
                    author_id = PostAuthor(name=author_name, profile_page=author_profile_page)
                    author_id.save()
                else:
                    twitter_page, linkedin_page, crunchbase_page = "", "", ""
                    about_author = author_profile_section.find("div", class_="author-profile__description").p
                    about_author = about_author.string if about_author != None and about_author.string != None else ""
                    social_links = author_profile_section.find("div", class_="author-profile__social-links")
                    
                    for link in social_links.find_all("a"):
                        if link != None:
                            if link.get("href").__contains__("twitter"):
                                twitter_page = link.get("href")
                            elif link.get("href").__contains__("crunchbase"):
                                crunchbase_page = link.get("href")
                            elif link.get("href").__contains__("linkedin"):
                                linkedin_page = link.get("href")

                    author_id = PostAuthor(name=author_name, profile_page=author_profile_page, about_author=about_author, twitter_page=twitter_page, crunchbase_page=crunchbase_page, linkedin_page=linkedin_page)
                    author_id.save()
                    count_new_authors += 1
                authors_ids = PostAuthor.objects.all()

            get_author_id = PostAuthor.objects.filter(name=author_name)
            dic_article = {
                'title': title,
                'website': content.a.get("href") if content.a != None else None,
                'authors': get_author_id[0] if get_author_id else None,
                'author_name': get_author_id[0].name if get_author_id else None,
                'author_profile_page': author_profile_page,
                'publication_date': publication_date,
                'description': description,
                'image_path': content.img.get("src") if content.find("img") != None else "",
                }

            article_id = PostArticle(title=dic_article['title'], website=dic_article['website'], author=dic_article['authors'], author_name=dic_article['author_name'], author_profile_page=dic_article['author_profile_page'], publication_date=dic_article['publication_date'], description=dic_article['description'], image_path=dic_article['image_path'])
            article_id.save()
            count_new_articles += 1
            articles_ids = PostArticle.objects.all()

        # force leave "for" because there is already the article
        if force_leave and look_new_articles:
            break

        # limitind get articles for the first three pages when access for the first time
        if soup.body.find(class_="load-more").get("href").__contains__(str(max_pages_first)) and not look_new_articles:
            break

        if not soup.body.find(class_="load-more").get("href").__contains__(str(max_pages_first)):
            load_more_page = soup.body.find(class_="load-more").get("href")
            page = requests.get(load_more_page)
            
    return count_new_articles, count_new_authors

# Create your views here.
@require_http_methods(["GET"])
def home(response):
    count_new_articles, count_new_authors = 0, 0
    try:
        count_new_articles, count_new_authors = search_for_articles()
    except:
        return render(response, 'main/page_not_found.html', {'error': "Refresh the page!", 'count_new_articles': count_new_articles, 'count_new_authors': count_new_authors})

    info_search_articles_box = response.GET.get('search_articles_box', None)
    if info_search_articles_box not in [None, ""]:
        articles_ids = search_articles_box(info_search_articles_box)
    else:
        articles_ids = PostArticle.objects.order_by('-publication_date', '-id')
    return render(response, "main/index.html", {'articles_ids': articles_ids, 'count_new_articles': count_new_articles, 'count_new_authors': count_new_authors})

def search_articles_box(info):
    articles_ids = PostArticle.objects.filter(title__icontains=info)
    return articles_ids

def show_all_articles_json(requests):
    try:
        search_for_articles()
    except:
        return JsonResponse([{'error': "Refresh the page!"}], json_dumps_params={'indent': 4}, safe=False)

    articles_ids = PostArticle.objects.order_by('-publication_date', '-id')
    return JsonResponse(list(articles_ids.values('title', 'website', 'author', 'author_name', 'author_profile_page', 'publication_date', 'description', 'image_path', 'id')), json_dumps_params={'indent': 4}, safe=False)

def show_this_article_json(requests, article_id):
    try:
        search_for_articles()
    except:
        return JsonResponse([{'error': "Refresh the page!"}], json_dumps_params={'indent': 4}, safe=False)

    articles_ids = PostArticle.objects.filter(id=article_id)
    return JsonResponse(list(articles_ids.values('title', 'website', 'author', 'author_name', 'author_profile_page', 'publication_date', 'description', 'image_path', 'id')), json_dumps_params={'indent': 4}, safe=False)

@require_http_methods(["GET"])
def show_authors(response):
    count_new_articles, count_new_authors = 0, 0
    try:
        count_new_articles, count_new_authors = search_for_articles()
    except:
        return render(response, 'main/page_not_found.html', {'error': "Refresh the page!", 'count_new_articles': count_new_articles, 'count_new_authors': count_new_authors})

    info_search_articles_box = response.GET.get('search_articles_box', None)
    if info_search_articles_box in [None]:
        authors_ids = PostAuthor.objects.order_by('name')
        return render(response, "main/authors.html", {'authors_ids': authors_ids, 'count_new_articles': count_new_articles, 'count_new_authors': count_new_authors})

    if info_search_articles_box not in [""]:
        articles_ids = search_articles_box(info_search_articles_box)
    else:
        articles_ids = PostArticle.objects.order_by('-publication_date', '-id')
    return render(response, "main/index.html", {'articles_ids': articles_ids, 'count_new_articles': count_new_articles, 'count_new_authors': count_new_authors})    

def show_this_author(response, author_id):
    count_new_articles, count_new_authors = 0, 0
    try:
        count_new_articles, count_new_authors = search_for_articles()
    except:
        return render(response, 'main/page_not_found.html', {'error': "Refresh the page!", 'count_new_articles': count_new_articles, 'count_new_authors': count_new_authors})

    info_search_articles_box = response.GET.get('search_articles_box', None)
    if info_search_articles_box in [None]:
        authors_ids = PostAuthor.objects.filter(id=author_id)
        return render(response, "main/authors.html", {'authors_ids': authors_ids, 'count_new_articles': count_new_articles, 'count_new_authors': count_new_authors})

    if info_search_articles_box not in [""]:
        articles_ids = search_articles_box(info_search_articles_box)
    else:
        articles_ids = PostArticle.objects.order_by('-publication_date', '-id')
    return render(response, "main/index.html", {'articles_ids': articles_ids, 'count_new_articles': count_new_articles, 'count_new_authors': count_new_authors})

def show_all_articles_author(response, author_id):
    count_new_articles, count_new_authors = 0, 0
    try:
        count_new_articles, count_new_authors = search_for_articles()
    except:
        return render(response, 'main/page_not_found.html', {'error': "Refresh the page!", 'count_new_articles': count_new_articles, 'count_new_authors': count_new_authors})

    info_search_articles_box = response.GET.get('search_articles_box', None)
    if info_search_articles_box in [None]:
        articles_ids = PostArticle.objects.filter(author=author_id).order_by('-publication_date', '-id')
        return render(response, "main/index.html", {'articles_ids': articles_ids, 'count_new_articles': count_new_articles, 'count_new_authors': count_new_authors})

    if info_search_articles_box not in [""]:
        articles_ids = search_articles_box(info_search_articles_box)
    else:
        articles_ids = PostArticle.objects.order_by('-publication_date', '-id')
    return render(response, "main/index.html", {'articles_ids': articles_ids, 'count_new_articles': count_new_articles, 'count_new_authors': count_new_authors})

def show_all_articles_author_json(requests, author_id):
    try:
        search_for_articles()
    except:
        return JsonResponse([{'error': "Refresh the page!"}], json_dumps_params={'indent': 4}, safe=False)

    articles_ids = PostArticle.objects.filter(author=author_id).order_by('-publication_date', '-id')
    return JsonResponse(list(articles_ids.values('title', 'website', 'author', 'author_name', 'author_profile_page', 'publication_date', 'description', 'image_path', 'id')), json_dumps_params={'indent': 4}, safe=False)

def show_all_authors_json(requests):
    try:
        search_for_articles()
    except:
        return JsonResponse([{'error': "Refresh the page!"}], json_dumps_params={'indent': 4}, safe=False)

    authors_ids = PostAuthor.objects.order_by('name')
    return JsonResponse(list(authors_ids.values('name', 'profile_page', 'about_author', 'twitter_page', 'linkedin_page', 'crunchbase_page', 'id')), json_dumps_params={'indent': 4}, safe=False)

def show_this_author_json(requests, author_id):
    try:
        search_for_articles()
    except:
        return JsonResponse([{'error': "Refresh the page!"}], json_dumps_params={'indent': 4}, safe=False)

    authors_ids = PostAuthor.objects.filter(id=author_id)
    return JsonResponse(list(authors_ids.values('name', 'profile_page', 'about_author', 'twitter_page', 'linkedin_page', 'crunchbase_page', 'id')), json_dumps_params={'indent': 4}, safe=False)

def exist_article(article_name):
    return bool(list(filter(lambda article: article.title.__contains__(article_name), PostArticle.objects.all())))

def exist_author(author_name):
    return bool(list(filter(lambda author: author.name == author_name, PostAuthor.objects.all())))

def show_page_not_found(response, info):
    error = info[0] if str(type(info)).__contains__("list") and info else "Page not found. Looks like you followed a bad link."
    return render(response, 'main/page_not_found.html', {'error': error})