from django.urls import path, re_path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # articles
    path('', views.home, name='home'),
    path('articles/', views.show_all_articles_json, name='show_all_articles_json'),
    path('search_articles/', views.home, name='search_articles'),
    path('articles/<int:article_id>', views.show_this_article_json, name='show_this_article_json'),

    #authors
    path('authors/', views.show_authors, name='authors'),
    path('authors/<int:author_id>', views.show_this_author, name='show_this_author'),
    path('authors/<int:author_id>/articles/', views.show_all_articles_author, name='show_all_articles_author'),
    path('authors/<int:author_id>/articles_json/', views.show_all_articles_author_json, name='show_all_articles_author_json'),
    path('authors_json/', views.show_all_authors_json, name='show_all_authors_json'),
    path('authors_json/<int:author_id>', views.show_this_author_json, name='show_this_author_json'),

    # pages not found
    re_path(r'()/', views.show_page_not_found, name='show_page_not_found'),
]
