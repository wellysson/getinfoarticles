>>>>>>>>>>  Observações <<<<<<<<<<<

- Este site feito em django tem como finalidade acessar o site https://techcrunch.com/ e obter as
informações dos artigos e seus autores para serem exibidas de uma forma normal como em json , o
link do site é https://getinfoarticles.herokuapp.com/ , este foi criado através deste projeto, até
o presente momento este projeto encontra-se finalizado.

- o site é atualizado a cada 2 minutos;

- existe um contador de artigos novos e autores recém cadastrados, esses contadores aparecem no
menu superior e serão zeradas na próxima atualização.

- o design do site é responsivo;

- Faltaram ainda algumas coisas, mas tentei compensar o tempo gasto a mais pensando na qualidade
e em funcionalidades interessantes, tentando também não deixar nenhuma falha para o usuário que
for visualizar;
